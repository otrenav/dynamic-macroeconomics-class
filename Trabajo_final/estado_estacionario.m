function F = SS(k, alpha, beta, gamma, delta, A)
    l = k*((1 - beta*(1 - delta))/(alpha*beta))^(1/(1 - alpha));
    c = k^alpha*l^(1 - alpha) - delta*k;
    F = gamma*c*l^alpha - (1 - gamma)*(1 - l)*(1 - alpha)*k^alpha; 
end