
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Dynamic macroeconomics class

- Omar Trejo
- March, 2015

I developed this Matlab code for the Dynamic Macroeconomics class at ITAM.

---

> "The best ideas are common property."
>
> —Seneca
