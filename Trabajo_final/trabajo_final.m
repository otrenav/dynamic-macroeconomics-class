%
% Trabajo final - Macroeconom�a Din�mica
% Profesor: Carlos Urrutia 
% ITAM,  2015 
%
% Equipo:
% Omar Trejo         119711
% Alejandro Cerecero 86979
% Arturo Reynoso     151613
%
%
% NOTA: Es importante correr el c�digo en orden. Cada secci�n que
%       debe ser ejecutada en conjunto se marca con un comentario
%       de secci�n (al menos dos %% seguidos al inicio de la l�nea).
%       Se deben ejecutar incisos completos, y s�lo un inciso a la vez.
%
%       El c�digo tarda en terminar alrededor de 8 minutos con una
%       computadora de 32 GB de RAM y 8 procesadores. Por lo que
%       puede tardar bastante a la hora de ser ejecutado. Para disminuir
%       el tiempo de ejecuci�n se podr�a trabajar con mallas m�s peque�as
%       a costa de p�rdida de precisi�n en los resultados. Con mallas
%       de tama�o menor a 400 los resultados resultan in�tiles.
%

%% Limpiar memoria

clear all
close all
clc

%% Par�metros del modelo

A     = 1;
alpha = 0.40;
beta  = 0.987;
delta = 0.012;
gamma = 0.64;
tau   = 0;

%% Par�metros de la soluci�n

maxit = 1000;      % Iteraciones m�ximas
p     = 500;       % Tama�o de malla de k
crit  = 1e-2;      % Tolerancia
r1    = 2000;      % Tama�o de primera malla de l
r2    = 500;       % Tama�o de segunda malla de l

%%%%%%%%%%%%%%%    Inciso (a) - Planificador Social    %%%%%%%%%%%%%%%

% Nota: se ajust� la matriz Pi para que sumara 1

Pi    = [0.9727 0.0273 0      0      0; 
         0.0041 0.9806 0.0153 0      0; 
         0      0.0081 0.9837 0.0082 0; 
         0      0      0.0153 0.9806 0.0041; 
         0      0      0      0.0273 0.9727];

z = [-0.0231, -0.0115,  0,  0.0115,  0.0231];
q = length(z);

% Estado estacionario

k0           = alpha*beta*A/(1 - beta*(1 - delta));
[kss, F_val] = fsolve( @(k)estado_estacionario(k, alpha, beta, gamma, delta, A), [k0]);
k0           = kss;
lss          = kss*((1 - beta*(1 - delta))/(alpha*beta))^(1/(1 - alpha));
k            = linspace(0.5*kss, 1.5*kss, p); 

% Funci�n de utilidad, producci�n y C.P.O. en cada (k, k', z)
CPO          = @(k, kk, L, z) (gamma*(exp(z)*k^alpha*L.^(1 - alpha) + (1 - delta)*k - kk)) /((1 - gamma).*(1 - L)) - (1 - alpha)* exp(z)*(k./L).^alpha;
F_utilidad   = @(k, kk, L, z) ((1 - gamma)*log(max((exp(z)*(k^(alpha))* L.^(1 - alpha) + (1 - delta)*k) - kk, 1e-200)) + gamma*log(max(1 - L, 1e-200)));
F_produccion = @(k, L, z)     (exp(z)*(k^(alpha))*L^(1 - alpha));

L_opt    = zeros([p p q]);
L_malla1 = linspace(0, 1, r1);
radio    = L_malla1(2) - L_malla1(1);

for m = 1:p
    for i = 1:q
        for j = 1:p
            %Primera Malla para el Trabajo
            utilidad_malla1 = F_utilidad(k(m), k(j), L_malla1, z(i));
            [~, ind] = max(utilidad_malla1);
            
            %Segunda Malla del Trabajo
            L_malla2 = linspace( L_malla1(ind) - radio, L_malla1(ind) + radio, r2);
            utilidad_malla2 = F_utilidad(k(m), k(j), L_malla2, z(i));
            [~, ind] = max(utilidad_malla2);
            L_opt(m, j, i) = L_malla2(ind);   
            
            if L_opt(m, j, i) >=  1
                L_opt(m, j, i) = 1 - 1e-100;
            end
            if L_opt(m, j, i) < 0
                L_opt(m, j, i) = 0;
            end
        end
    end
    disp(['LLenado de mallas: ', num2str(m), '/', num2str(p)]);
end

%
% Construcci�n de la Matriz M
%

M = 0;
M_aux = zeros(q, p);
for i = 1:q
    for j = 1:p
        M_aux(i, j) = F_utilidad(k(1), k(j), L_opt(1, j, i), z(i));
    end
end
M = M_aux;
for m = 2:p
    for i = 1:q
        for j = 1:p
            M_aux(i, j) = F_utilidad(k(m), k(j), L_opt(m, j, i), z(i));
        end
    end
    M = [M; M_aux];
end

fprintf('\n');

I = eye(q);
E = I;
for i = 1:p - 1
    E = [E; I];
end

%
% Iteraci�n de la funci�n valor
%

V0   = zeros(p*q, 1);
term = 0;
iter = 1;

while term == 0 && iter < maxit
    aux = M + beta*E*Pi*reshape(V0, q, p);
    [V1, G] = max(aux');
    V1 = V1';
    G = G';
    if norm(V1 - V0) < crit
        term = 1;
    end
    disp(['Iter = ',  num2str(iter), ...
          ' ||V0 - V1|| = ', num2str(norm(V0 - V1))]);
    V0 = V1;
    iter = iter + 1;
end

%
% Extracci�n de resultados
%

V = reshape(V1, q, p)';
G = reshape(G, q, p)';

for j = 1:5
    for i = 1:p
        eval(['c_' num2str(j) '(i) = F_produccion(k(i), L_opt(i, G(i, j), j), z(j)) - k(G(i, j)) + (1 - delta)*k(i);'])
        eval(['trabajo_' num2str(j) '(i) = L_opt(i, G(i, j), j);'])
    end
end

%
% Gr�ficas
%

figure(1)

subplot(4, 1, 1)
plot(k, V(:, 1), 'b', ...
     k, V(:, 2), 'r', ...
     k, V(:, 3), 'g', ...
     k, V(:, 4), 'c', ...
     k, V(:, 5), 'black')
legend('z = -0.0231', ...
       'z = -0.0115', ...
       'z = 0', ...
       'z = 0.0115', ...
       'z = 0.0231')
title('Funci$\acute{o}$n valor', ...
      'interpreter', 'latex')
ylabel('v(k, z)')
xlabel('k')

subplot(4, 1, 2)
plot(k, c_1, 'b', ...
     k, c_2, 'r', ...
     k, c_3, 'g', ...
     k, c_4, 'c', ...
     k, c_5, 'black')
legend('z = -0.0231', ...
       'z = -0.0115', ...
       'z = 0', ...
       'z = 0.0115', ...
       'z = 0.0231')
title('Consumo')
ylabel('c(k, z)')
xlabel('k')

subplot(4, 1, 3)
plot(k, k(G(:, 1)), 'b', ...
     k, k(G(:, 2)), 'r', ...
     k, k(G(:, 3)), 'g', ...
     k, k(G(:, 4)), 'c', ...
     k, k(G(:, 5)), 'black'), 
legend('z = -0.0231', ...
       'z = -0.0115', ...
       'z = 0', ...
       'z = 0.0115', ...
       'z = 0.0231')
title('Capital')
ylabel('k�(k, z)')
xlabel('k')

subplot(4, 1, 4)
plot(k, trabajo_1, 'b', ...
     k, trabajo_2, 'r', ...
     k, trabajo_3, 'g', ...
     k, trabajo_4, 'c', ...
     k, trabajo_5, 'black')
legend('z = -0.0231', ...
       'z = -0.0115', ...
       'z = 0', ...
       'z = 0.0115', ...
       'z = 0.0231')
title('Trabajo')
ylabel('l(k, z)')
xlabel('k') 

%%%%%%%%%%%%%%%    Inciso (b) - Estad�sticas    %%%%%%%%%%%%%%%

% 100 simulaciones de 10,000 periodos cada una

T_sim = 10000;
for m = 1:100
    disp(['Simulacion: ', num2str(m), '/', num2str(100)]);
    s0          = 3;  % Incialmente z = 0
    [~, state]  = markov(Pi, T_sim, s0, z);
    [~, ind]    = max(state);
    choques_sim = [3 ind]';

    % Trayectorias �ptimas
    kt_sim            = zeros(T_sim + 1, 1);
    ind_sim           = zeros(T_sim, 1);
    [aux, ind_sim(1)] = min(k < k0);
    kt_sim(1)         = k(ind_sim(1));

    for t = 1:T_sim
        ind_sim(t + 1) = G(ind_sim(t), choques_sim(t));  
        kt_sim(t + 1)  = k(ind_sim(t + 1)); 
        lt_sim(t)      = L_opt(ind_sim(t), ind_sim(t + 1), choques_sim(t));
    end

    it_sim = zeros(T_sim, 1);
    ct_sim = zeros(T_sim, 1);
    yt_sim = zeros(T_sim, 1);

    for t = 1:T_sim
        it_sim(t) = kt_sim(t + 1) - (1 - delta)*kt_sim(t);
        yt_sim(t) = F_produccion(kt_sim(t), lt_sim(t), z(choques_sim(t)));
        ct_sim(t) = yt_sim(t) - it_sim(t);
    end
    
    %
    % SD y las Corr sin primeras 1,000 observaciones
    %
    kt_desv(m) = std(log(kt_sim(1000:10000)));
    yt_desv(m) = std(log(yt_sim(1000:10000)));
    ct_desv(m) = std(log(ct_sim(1000:10000)));
    it_desv(m) = std(log(it_sim(1000:10000)));
    lt_desv(m) = std(log(lt_sim(1000:10000)));

    kt_yt_corr(m) = corr(kt_sim(1000:10000), yt_sim(1000:10000));
    ct_yt_corr(m) = corr(ct_sim(1000:10000), yt_sim(1000:10000));
    it_yt_corr(m) = corr(it_sim(1000:10000), yt_sim(1000:10000));
    lt_yt_corr(m) = corr((lt_sim(1000:10000))', yt_sim(1000:10000));
end

% 
% Estad�sticas
%
disp(' ')
disp('Desviaci�n est�ndar del logaritmo de: ')
disp(['Producci�n = ', num2str(mean(yt_desv), 16)])
disp(['Consumo    = ', num2str(mean(ct_desv), 16)])
disp(['Inversi�n  = ', num2str(mean(it_desv), 16)])
disp(['Capital    = ', num2str(mean(kt_desv), 16)])
disp(['Trabajo    = ', num2str(mean(lt_desv), 16)])
disp(' ')
disp('Correlaci�n de la producci�n con: ')
disp(['Consumo   = ', num2str(mean(ct_yt_corr), 16)])
disp(['Inversi�n = ', num2str(mean(it_yt_corr), 16)])
disp(['Capital   = ', num2str(mean(kt_yt_corr), 16)])
disp(['Trabajo   = ', num2str(mean(lt_yt_corr), 16)])

%%%%%%%%%%%%%%%    Inciso (c) - Funciones impulso-respuesta    %%%%%%%%%%%%%%%

% Choque positivo de una desviaci�n est�ndar y
% despu�s volvemos a estado sin choqque (z = 0).

T_ir     = 20;
zt_ir    = ones(T_ir , 1)*3;  % Por default z = 0
zt_ir(2) = 4;                 % Primer periodo incrementa a z = 0.0115

% Generamos los choques

kt_ir          = zeros(T_ir  + 1, 1); 
ind_ir         = zeros(T_ir, 1);
[~, ind_ir(1)] = min(k < kss);
kt_ir(1)       = k(ind_ir(1));
lt_ir          = zeros(T_ir, 1);

for t = 1:T_ir
    ind_ir(t + 1) = G(ind_ir(t), zt_ir(t));  
    kt_ir(t + 1)  = k(ind_ir(t + 1));
    lt_ir(t)      = L_opt(ind_ir(t), ind_ir(t + 1), zt_ir(t));
end

it_ir = zeros(T_ir, 1);
ct_ir = zeros(T_ir, 1);
yt_ir = zeros(T_ir, 1);

for t = 1:T_ir
    it_ir(t) = kt_ir(t + 1) - (1 - delta)*kt_ir(t);
    yt_ir(t) = F_produccion(kt_ir(t), lt_ir(t), z(zt_ir(t)));
    ct_ir(t) = yt_ir(t) - it_ir(t);
end

%
% Gr�ficas
%

figure(2)

subplot(5, 1, 1)
plot(1:T_ir, yt_ir)
title('Producci�n')
ylabel('y(t)')
xlabel('t')  

subplot(5, 1, 2)
plot(1:T_ir, ct_ir)
title('Consumo')
ylabel('c(t)')
xlabel('t')  

subplot(5, 1, 3)
plot(1:T_ir, it_ir)
title('Inversi�n')
ylabel('i(t)')
xlabel('t')  

subplot(5, 1, 4)
plot(1:T_ir, kt_ir(1:T_ir))
title('Capital')
ylabel('k(t)')
xlabel('t')  

subplot(5, 1, 5)
plot(1:T_ir, lt_ir)
title('Trabajo')
ylabel('l(t)')
xlabel('t')

%%%%%%%%%%%%%%%    Inciso (d) - Cambio de par�metros    %%%%%%%%%%%%%%%

% Ejecutamos el mismo modelo del inciso (a) pero cambiamos gamma para
% provocar un cambio del trabajo de estado estacionario a un nivel de
% 0.45, de acuerdo a lo publicado por la OECD (2013).

%% Par�metros del modelo

A     = 1;
alpha = 0.40;
beta  = 0.987;
delta = 0.012;
gamma = 0.64;
tau   = 0;

%% Par�metros de la soluci�n

maxit = 1000;      % Iteraciones m�ximas
p     = 500;       % Tama�o de malla de k
crit  = 1e-2;      % Tolerancia
r1    = 2000;      % Tama�o de primera malla de l
r2    = 500;       % Tama�o de segunda malla de l

%% Calibraci�n de gamma_ajustada y tau_ajustada

% Estado estacionario
lss      = 1/(((gamma + ...
                (1 - alpha)*(1 - tau)*(1 - gamma))*(1 - beta*(1 - delta)) - ...
               alpha*beta*delta*gamma) / ...
              ((1 - alpha)*(1 - gamma)*(1 - tau)*(1 - beta*(1 - delta))));

kss      = lss*((A*beta*alpha)/((1 - beta*(1 - delta))))^(1/(1 - alpha));
yss      = A*(kss^alpha)*(lss^(1 - alpha));
iss      = delta*kss;
css      = yss - iss;
ratio_kl = kss/lss;

%% lss = 0.45 moviendo gamma

% Condici�n de eficiencia del mercado laboral para obtener 
% el valor de gamma que resulta en lss = 0.45 (OECD, 2013), 
% manteniendo k/l constante.

lss_gamma = 0.45;
kss_gamma = lss_gamma*((A*beta*alpha)/((1 - beta*(1 - delta))))^(1/(1 - alpha));
gamma_ajustada = fsolve(@(gamma_ajustada)eficiencia_laboral(gamma_ajustada, ...
                                                  tau, ...
                                                  lss_gamma, ...
                                                  kss_gamma, ...
                                                  A, ...
                                                  alpha, ...
                                                  delta), gamma);

% Estado estacionario con nuevo valor de gamma
yss_gamma      = A*(kss_gamma^alpha)*(lss_gamma^(1-alpha));
iss_gamma      = delta*kss_gamma;
css_gamma      = yss_gamma-iss_gamma;
ratio_kl_gamma = kss_gamma/lss_gamma;

%% lss = 0.45 moviendo tau

% Condicion de eficiencia del mercado laboral para obtener
% el valor de tau que resulta en lss = 0.45 (OECD, 2013), 
% manteniendo k/l constante.

lss_tau = lss_gamma;
kss_tau = kss_gamma;
tau_ajustada = fsolve(@(tau_ajustada)eficiencia_laboral(tau_ajustada, ...
                                                  lss_tau, ...
                                                  kss_tau, ...
                                                  A, ...
                                                  alpha, ...
                                                  delta, ...
                                                  gamma), tau);

% Estado estacionario con nuevo valor de tau
yss_tau      = A*(kss_tau^alpha)*(lss_tau^(1 - alpha));
iss_tau      = delta*kss_gamma;
css_tau      = yss_tau - iss_tau;
ratio_kl_tau = kss_gamma/lss_gamma;

% Nota: Nuestro resultado es consistente con Prescott (2004).

%
% Relaci�n entre tau y lss
%
grid = 100;
lssg = zeros(grid, 1);
taus = linspace(-1.4, 0.3, grid);

for i = 1:grid
    lssg(i) = 1/(((gamma + ...
                   (1 - alpha)*(1 - taus(i))*(1 - gamma))*(1 - beta*(1 - delta)) - ...
                  alpha*beta*delta*gamma) / ...
                 ((1 - alpha)*(1 - gamma)*(1 - taus(i))*(1 - beta*(1 - delta))));
end

%
% Relaci�n entre tau y lss
%
plot(taus, lssg);
ylabel('lss');
xlabel('tau');
axis([-1.4 0.3 0.226 0.5]);

% Efectivamente la el ratio entre kss y lss se mantiene 
% constante ya que ratio_kl_tau == ratio_kl_gamma.

%%%%%%%%%%%%%%%    Inciso (d) - Ejecuci�n del modelo con nueva gamma    %%%%%%%%%%%%%%%

% Nota: se ajust� la matriz Pi para que sumara 1

gamma = 0.428;  % Nueva gamma

Pi    = [0.9727 0.0273 0      0      0; 
         0.0041 0.9806 0.0153 0      0; 
         0      0.0081 0.9837 0.0082 0; 
         0      0      0.0153 0.9806 0.0041; 
         0      0      0      0.0273 0.9727];

z = [-0.0231, -0.0115,  0,  0.0115,  0.0231];
q = length(z);

% Estado estacionario

k0           = alpha*beta*A/(1 - beta*(1 - delta));
[kss, F_val] = fsolve( @(k)estado_estacionario(k, alpha, beta, gamma, delta, A), [k0]);
k0           = kss;
lss          = kss*((1 - beta*(1 - delta))/(alpha*beta))^(1/(1 - alpha));
k            = linspace(0.5*kss, 1.5*kss, p); 

% Funci�n de utilidad, producci�n y C.P.O. en cada (k, k', z)
CPO          = @(k, kk, L, z) (gamma*(exp(z)*k^alpha*L.^(1 - alpha) + (1 - delta)*k - kk)) /((1 - gamma).*(1 - L)) - (1 - alpha)* exp(z)*(k./L).^alpha;
F_utilidad   = @(k, kk, L, z) ((1 - gamma)*log(max((exp(z)*(k^(alpha))* L.^(1 - alpha) + (1 - delta)*k) - kk, 1e-200)) + gamma*log(max(1 - L, 1e-200)));
F_produccion = @(k, L, z)     (exp(z)*(k^(alpha))*L^(1 - alpha));

L_opt    = zeros([p p q]);
L_malla1 = linspace(0, 1, r1);
radio    = L_malla1(2) - L_malla1(1);

for m = 1:p
    for i = 1:q
        for j = 1:p
            %Primera Malla para el Trabajo
            utilidad_malla1 = F_utilidad(k(m), k(j), L_malla1, z(i));
            [~, ind] = max(utilidad_malla1);
            
            %Segunda Malla del Trabajo
            L_malla2 = linspace( L_malla1(ind) - radio, L_malla1(ind) + radio, r2);
            utilidad_malla2 = F_utilidad(k(m), k(j), L_malla2, z(i));
            [~, ind] = max(utilidad_malla2);
            L_opt(m, j, i) = L_malla2(ind);   
            
            if L_opt(m, j, i) >=  1
                L_opt(m, j, i) = 1 - 1e-100;
            end
            if L_opt(m, j, i) < 0
                L_opt(m, j, i) = 0;
            end
        end
    end
    disp(['LLenado de mallas: ', num2str(m), '/', num2str(p)]);
end

%
% Construcci�n de la Matriz M
%

M = 0;
M_aux = zeros(q, p);
for i = 1:q
    for j = 1:p
        M_aux(i, j) = F_utilidad(k(1), k(j), L_opt(1, j, i), z(i));
    end
end
M = M_aux;
for m = 2:p
    for i = 1:q
        for j = 1:p
            M_aux(i, j) = F_utilidad(k(m), k(j), L_opt(m, j, i), z(i));
        end
    end
    M = [M; M_aux];
end

fprintf('\n');

I = eye(q);
E = I;
for i = 1:p - 1
    E = [E; I];
end

%
% Iteraci�n de la funci�n valor
%

V0   = zeros(p*q, 1);
term = 0;
iter = 1;

while term == 0 && iter < maxit
    aux = M + beta*E*Pi*reshape(V0, q, p);
    [V1, G] = max(aux');
    V1 = V1';
    G = G';
    if norm(V1 - V0) < crit
        term = 1;
    end
    disp(['Iter = ',  num2str(iter), ...
          ' ||V0 - V1|| = ', num2str(norm(V0 - V1))]);
    V0 = V1;
    iter = iter + 1;
end

%
% Extracci�n de resultados
%

V = reshape(V1, q, p)';
G = reshape(G, q, p)';

for j = 1:5
    for i = 1:p
        eval(['c_' num2str(j) '(i) = F_produccion(k(i), L_opt(i, G(i, j), j), z(j)) - k(G(i, j)) + (1 - delta)*k(i);'])
        eval(['trabajo_' num2str(j) '(i) = L_opt(i, G(i, j), j);'])
    end
end

%
% Gr�ficas
%

figure(3)

subplot(4, 1, 1)
plot(k, V(:, 1), 'b', ...
     k, V(:, 2), 'r', ...
     k, V(:, 3), 'g', ...
     k, V(:, 4), 'c', ...
     k, V(:, 5), 'black')
legend('z = -0.0231', ...
       'z = -0.0115', ...
       'z = 0', ...
       'z = 0.0115', ...
       'z = 0.0231')
title('Funci$\acute{o}$n valor', ...
      'interpreter', 'latex')
ylabel('v(k, z)')
xlabel('k')

subplot(4, 1, 2)
plot(k, c_1, 'b', ...
     k, c_2, 'r', ...
     k, c_3, 'g', ...
     k, c_4, 'c', ...
     k, c_5, 'black')
legend('z = -0.0231', ...
       'z = -0.0115', ...
       'z = 0', ...
       'z = 0.0115', ...
       'z = 0.0231')
title('Consumo')
ylabel('c(k, z)')
xlabel('k')

subplot(4, 1, 3)
plot(k, k(G(:, 1)), 'b', ...
     k, k(G(:, 2)), 'r', ...
     k, k(G(:, 3)), 'g', ...
     k, k(G(:, 4)), 'c', ...
     k, k(G(:, 5)), 'black'), 
legend('z = -0.0231', ...
       'z = -0.0115', ...
       'z = 0', ...
       'z = 0.0115', ...
       'z = 0.0231')
title('Capital')
ylabel('k�(k, z)')
xlabel('k')

subplot(4, 1, 4)
plot(k, trabajo_1, 'b', ...
     k, trabajo_2, 'r', ...
     k, trabajo_3, 'g', ...
     k, trabajo_4, 'c', ...
     k, trabajo_5, 'black')
legend('z = -0.0231', ...
       'z = -0.0115', ...
       'z = 0', ...
       'z = 0.0115', ...
       'z = 0.0231')
title('Trabajo')
ylabel('l(k, z)')
xlabel('k') 

%% Volvemos a ejecutar el inciso (b) con los cambios de gamma

% 100 simulaciones de 10,000 periodos cada una

T_sim = 10000;
for m = 1:100
    disp(['Simulacion: ', num2str(m), '/', num2str(100)]);
    s0          = 3;  % Incialmente z = 0
    [~, state]  = markov(Pi, T_sim, s0, z);
    [~, ind]    = max(state);
    choques_sim = [3 ind]';

    % Trayectorias �ptimas
    kt_sim            = zeros(T_sim + 1, 1);
    ind_sim           = zeros(T_sim, 1);
    [aux, ind_sim(1)] = min(k < k0);
    kt_sim(1)         = k(ind_sim(1));

    for t = 1:T_sim
        ind_sim(t + 1) = G(ind_sim(t), choques_sim(t));  
        kt_sim(t + 1)  = k(ind_sim(t + 1)); 
        lt_sim(t)      = L_opt(ind_sim(t), ind_sim(t + 1), choques_sim(t));
    end

    it_sim = zeros(T_sim, 1);
    ct_sim = zeros(T_sim, 1);
    yt_sim = zeros(T_sim, 1);

    for t = 1:T_sim
        it_sim(t) = kt_sim(t + 1) - (1 - delta)*kt_sim(t);
        yt_sim(t) = F_produccion(kt_sim(t), lt_sim(t), z(choques_sim(t)));
        ct_sim(t) = yt_sim(t) - it_sim(t);
    end
    
    %
    % SD y las Corr sin primeras 1,000 observaciones
    %
    kt_desv(m) = std(log(kt_sim(1000:10000)));
    yt_desv(m) = std(log(yt_sim(1000:10000)));
    ct_desv(m) = std(log(ct_sim(1000:10000)));
    it_desv(m) = std(log(it_sim(1000:10000)));
    lt_desv(m) = std(log(lt_sim(1000:10000)));

    kt_yt_corr(m) = corr(kt_sim(1000:10000), yt_sim(1000:10000));
    ct_yt_corr(m) = corr(ct_sim(1000:10000), yt_sim(1000:10000));
    it_yt_corr(m) = corr(it_sim(1000:10000), yt_sim(1000:10000));
    lt_yt_corr(m) = corr((lt_sim(1000:10000))', yt_sim(1000:10000));
end

% 
% Estad�sticas
%
disp(' ')
disp('Desviaci�n est�ndar del logaritmo de: ')
disp(['Producci�n = ', num2str(mean(yt_desv), 16)])
disp(['Consumo    = ', num2str(mean(ct_desv), 16)])
disp(['Inversi�n  = ', num2str(mean(it_desv), 16)])
disp(['Capital    = ', num2str(mean(kt_desv), 16)])
disp(['Trabajo    = ', num2str(mean(lt_desv), 16)])
disp(' ')
disp('Correlaci�n de la producci�n con: ')
disp(['Consumo   = ', num2str(mean(ct_yt_corr), 16)])
disp(['Inversi�n = ', num2str(mean(it_yt_corr), 16)])
disp(['Capital   = ', num2str(mean(kt_yt_corr), 16)])
disp(['Trabajo   = ', num2str(mean(lt_yt_corr), 16)])
