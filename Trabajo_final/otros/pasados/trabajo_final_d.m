%
% Código para responder el inciso (d) 
%
% Trabajo final - Macroeconomía Dinámica
% Profesor: Carlos Urrutia 
% ITAM, 2015 
%
% Equipo:
% Alejandro Cerecero, 86979
% Arturo Reynoso, 151613
% Omar Trejo, 119711
%
% En este incisco calibramos un nivel de gamma y de impuestos que den 
% como  resultado las horas trabajadas en Mexico. Se supone un impuesto
% a los ingresos laborales, regresado a los agentes como transferencia 
% lumpsum.

%% Limpiar memoria

clear all
close all
clc

%% Parámetros del modelo

A     = 1;      % Productividad
alpha = 0.40;   % 
beta  = 0.987;  % Factor de descuento
delta = 0.012;  % 
gamma = 0.64;   %
tau   = 0;      % Impuesto al trabajo

%% Parámetros de la solución

max_iter = 1000;  % Iteraciones máximas
tol      = 1e-3;  % Tolerancia
p        = 50;    % Tamaño de la malla de k
m        = 50;    % Tamaño de la malla de l

%% Calibración de gamma_ajustada y tau_ajustada

% Estado estacionario
lss      = 1/(((gamma + ...
                (1 - alpha)*(1 - tau)*(1 - gamma))*(1 - beta*(1 - delta)) - ...
               alpha*beta*delta*gamma) / ...
              ((1 - alpha)*(1 - gamma)*(1 - tau)*(1 - beta*(1 - delta))));

kss      = lss*((A*beta*alpha)/((1 - beta*(1 - delta))))^(1/(1 - alpha));
yss      = A*(kss^alpha)*(lss^(1 - alpha));
iss      = delta*kss;
css      = yss - iss;
ratio_kl = kss/lss;

%% lss = 0.45 moviendo gamma

% Condición de eficiencia del mercado laboral para obtener 
% el valor de gamma que resulta en lss = 0.45 (OECD, 2013), 
% manteniendo k/l constante.

lss_gamma = 0.45;
kss_gamma = lss_gamma*((A*beta*alpha)/((1 - beta*(1 - delta))))^(1/(1 - alpha));
gamma_ajustada = fsolve(@(gamma_ajustada)eficiencia_laboral(gamma_ajustada, ...
                                                  tau, ...
                                                  lss_gamma, ...
                                                  kss_gamma, ...
                                                  A, ...
                                                  alpha, ...
                                                  delta), gamma);

% Estado estacionario con nuevo valor de gamma
yss_gamma      = A*(kss_gamma^alpha)*(lss_gamma^(1-alpha));
iss_gamma      = delta*kss_gamma;
css_gamma      = yss_gamma-iss_gamma;
ratio_kl_gamma = kss_gamma/lss_gamma;

%% lss = 0.45 moviendo tau

% Condicion de eficiencia del mercado laboral para obtener
% el valor de tau que resulta en lss = 0.45 (OECD, 2013), 
% manteniendo k/l constante.

lss_tau = lss_gamma;
kss_tau = kss_gamma;
tau_ajustada = fsolve(@(tau_ajustada)eficiencia_laboral(tau_ajustada, ...
                                                  lss_tau, ...
                                                  kss_tau, ...
                                                  A, ...
                                                  alpha, ...
                                                  delta, ...
                                                  gamma), tau);

% Estado estacionario con nuevo valor de tau
yss_tau      = A*(kss_tau^alpha)*(lss_tau^(1 - alpha));
iss_tau      = delta*kss_gamma;
css_tau      = yss_tau - iss_tau;
ratio_kl_tau = kss_gamma/lss_gamma;

%
% Nota: Nuestro resultado es consistente con Prescott (2004).
%

%
% Relación entre tau y lss
%
grid = 100;
lssg = zeros(grid, 1);
taus = linspace(-1.4, 0.3, grid);

for i = 1:grid
    lssg(i) = 1/(((gamma + ...
                   (1 - alpha)*(1 - taus(i))*(1 - gamma))*(1 - beta*(1 - delta)) - ...
                  alpha*beta*delta*gamma) / ...
                 ((1 - alpha)*(1 - gamma)*(1 - taus(i))*(1 - beta*(1 - delta))));
end

plot(taus, lssg);
axis([-1.4 0.3 0.226 0.5]);

% Efectivamente la el ratio entre kss y lss se mantiene constante.
% ya que ratio_kl_tau == ratio_kl_gamma.

%%%%%%%%%%%%%%%    Inciso (d)    %%%%%%%%%%%%%%%

%
% Ejecutamos el modelo con gamma = 0.4754
%
gamma = 0.428;

% lss debería de llegar a 0.45, pero llega a 0.3995.

% El máximo es tau = -3.7, después de eso se rompe el modelo.
% Con tau = -3.7 se llega a 0.4382 de empelo => algo está mal.
% Con tau = -0.90 se llega a 0.2947 de empleo.
% Con tau = 0.4 se llega a 0.2934 también en empleo.

% Alguna de las condiciones donde entra tau está mal?
% Ahorita sólo está afectando a través de lss.

% Cómo tiene que entrar la tau al modelo?

%
% Usamos la aproximación a AR(1) con el método de Tauchen (1986)
%

theta = [ -0.0231  -0.0115 0 0.0115 0.0231]';
q     = length(theta);

pi = [0.9727 0.0273      0      0      0;
      0.0041 0.9806 0.0153      0      0;
      0 0.0082 0.9837 0.0082      0;
      0      0 0.0153 0.9806 0.0041;
      0      0      0 0.0273 0.9727];

% Trabajo del estado estacionario
lss = 1/(((gamma + (1 - alpha)*(1 - tau)* ...
           (1 - gamma))*(1 - beta*(1 - delta)) - ...
          alpha*beta*delta*gamma) / ...
         ((1 - alpha)*(1 - gamma)*(1 - tau)*(1 - beta*(1 - delta))));

% Capital del estado estacionario
kss = lss*(alpha*beta*A/(1 - beta*(1 - delta)))^(1/(1 - alpha));

k = linspace(2/3*kss, 1.5*kss, p);

%% TODO (Omar) Alejandro sugiere regresar el método de resolver la 
% condición de eficiencia para la creación de la parte de l(t) de M.
l = linspace(2/3*lss, 1.5*lss, m);

value = zeros(p*q, p*m, 1);

% Matriz valor: k(t) (capital)
for i = 1:p
    for j = 1:q
        value((i - 1)*q + j, :, 1) = k(i)*ones(1, p*m);
    end
end

% Matriz valor: k(t+1) (capital)
kaux = k(1)*ones(p*q, m);
for i = 2:p
    kaux = [kaux, k(i)*ones(p*q, m)];
end
value(:, :, 2) = kaux;


% Matriz valor: z(t) (shock)
vector = theta;
for i = 1:p - 1
    vector = [vector; theta];
end
value(:, :, 3) = vector*ones(1, p*m);


% Matriz valor: l(t) (trabajo)
laux = l; 
for i = 1:m - 1
    laux = [laux,  l];
end
value(:, :, 4) = ones(p*q, 1)*laux;

% La matriz M guarda la función de retorno evaluada en 
% cada posible combinación de (k, z) hoy (variables de 
% estado) y (k mañana, l hoy) (variables de control).

% Como es una función de utilidad logarítmica 
% elminamos las celdas inalcanzables con max.

%% TODO (Omar) Qué debería de ser aquí?
M = (1 - gamma)*...
    log(max( ...
        exp(value(:, :, 3)).*A.*value(:, :, 1).^(alpha).*value(:, :, 4).^(1 - alpha) - ...
        value(:, :, 2) + (1 - delta).*value(:, :, 1) - ...
        exp(value(:, :, 3)).*A.*value(:, :, 1).^(alpha).*value(:, :, 4).^(1 - alpha)*alpha*tau, ...
        1e-100)) + gamma*log(max(1 - value(:, :, 4), 1e-100)); 
%% HASTA AQUI

I = eye(q);
E = I; 
for i = 1:p - 1
    E = [E; I];
end

%
% Ejecución del algoritmo
%

V0   = zeros(p*q, 1);
fin  = 0;
iter = 1;

while fin == 0 && iter < max_iter
    me = beta*(E*(pi*reshape(V0, q, p)));
    beta_aux = me(:, 1)*ones(1, m);
    for i = 2:p
        beta_aux = [beta_aux, me(:, i)*ones(1, m)];
    end
	[V1, G1] = max((M + beta_aux)');
	V1 = V1';
	G1 = G1';
    if norm(V0 - V1) < tol
        fin = 1;
    end
    disp(['Iter = ',  num2str(iter), ...
          ' ||V0 - V1|| = ', num2str(norm(V0 - V1))]);
    V0   = V1;
    iter = iter + 1;
end

%
% Extracción de resultados
%

G = zeros(p, q);
V = zeros(p, q);
H = zeros(m, q);

for i = 1:p
    for j = 1:q
        G(i, j) = floor((G1((i - 1)*q + j))/p);
        V(i, j) = V1((i - 1)*q + j);
    end
end

for i = 1:m
    for j = 1:q
        H(i, j) = mod(G1((i - 1)*q + j), m);
        if H(i, j) == 0
            H(i, j) = m;
        end
    end
end

for j = 1:q
    G(1, j) = 1; 
    V(1, j) = V(2, j);
end

% Consumo para cada estado
c_res = zeros(p, q);
for i = 1:p
    for j = 1:q
        c(i, j) = exp(theta(j))*A*k(i)^alpha*l(H(i, j))^(1 - alpha) - ...
                  k(G(i, j)) + (1 - delta)*k(i);
    end
end 

% Trabajo para cada estado
l_res = zeros(p, q);
for i = 1:p
    for j = 1:q
        l_res(i, j) = l(H(i, j));
    end
end

%
% Gráficas
%

figure(1)

subplot(4, 1, 1)
plot(k, k(G(:, 1)), 'b', ...
     k, k(G(:, 2)), 'r', ...
     k, k(G(:, 3)), 'g', ...
     k, k(G(:, 4)), 'c', ...
     k, k(G(:, 5)), 'm')
legend('theta =  - 0.0231', ...
       'theta =  - 0.0115', ...
       'theta = 0', ...
       'theta = 0.0115', ...
       'theta = 0.0231')
title('Capital $\acute{o}$ptimo', ...
      'FontSize', 15, ...
      'interpreter', 'latex')

subplot(4, 1, 2)
plot(k, V(:, 1), 'b', ...
     k, V(:, 2), 'r', ...
     k, V(:, 3), 'g', ...
     k, V(:, 4), 'c', ...
     k, V(:, 5), 'm')
legend('theta =  - 0.0231', ...
       'theta =  - 0.0115', ...
       'theta = 0', ...
       'theta = 0.0115', ...
       'theta = 0.0231')
title('Funci$\acute{o}$n Valor', ...
      'FontSize', 15, ...
      'interpreter', 'latex')

subplot(4, 1, 3)
plot(k, c(:, 1), 'b', ...
     k, c(:, 2), 'r', ...
     k, c(:, 3), 'g', ...
     k, c(:, 4), 'c', ...
     k, c(:, 5), 'm')
legend('theta =  - 0.0231', ...
       'theta =  - 0.0115', ...
       'theta = 0', ...
       'theta = 0.0115', ...
       'theta = 0.0231')
title('Consumo $\acute{O}$ptimo', ...
      'FontSize', 15, ...
      'interpreter', 'latex')

subplot(4, 1, 4)
plot(k, l_res(:, 1), 'b', ...
     k, l_res(:, 2), 'r', ...
     k, l_res(:, 3), 'g', ...
     k, l_res(:, 4), 'c', ...
     k, l_res(:, 5), 'm')
legend('theta =  - 0.0231', ...
       'theta =  - 0.0115', ...
       'theta = 0', ...
       'theta = 0.0115', ...
       'theta = 0.0231')
title('Trabajo $\acute{O}$ptimo', ...
      'FontSize', 15, ...
      'interpreter', 'latex')