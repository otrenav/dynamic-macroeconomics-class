%
% C�digo para responder los incisos (a)-(c) 
%
% Trabajo final - Macroeconom�a Din�mica
% Profesor: Carlos Urrutia 
% ITAM, 2015 
%
% Equipo:
% Alejandro Cerecero, 86979
% Arturo Reynoso, 151613
% Omar Trejo, 119711
%

%% Limpiar memoria

clear all
close all
clc

%% Par�metros del modelo

A     = 1;      % Productividad
alpha = 0.40;   % 
beta  = 0.987;  % Factor de descuento
delta = 0.012;  % 
gamma = 0.64;   % 
tau   = 0;      % Impuest al trabajo

%% Par�metros de la soluci�n

max_iter = 1000;  % Iteraciones m�ximas
tol      = 1e-3;  % Tolerancia
p        = 50;    % Tama�o de la malla de k
m        = 50;    % Tama�o de la malla de l

%%%%%%%%%%%%%%%    Inciso (a)    %%%%%%%%%%%%%%%

%
% Usamos la aproximaci�n a AR(1) con el m�todo de Tauchen (1986)
%

theta = [-0.0231 -0.0115 0 0.0115 0.0231]';
q     = length(theta);

Pi = [0.9727 0.0273 0      0      0;
      0.0041 0.9806 0.0153 0      0;
      0      0.0082 0.9837 0.0082 0;
      0      0      0.0153 0.9806 0.0041;
      0      0      0      0.0273 0.9727];

% Trabajo del estado estacionario
lss = 1/(((gamma + (1 - alpha)*(1 - tau)* ...
           (1 - gamma))*(1 - beta*(1 - delta)) - ...
          alpha*beta*delta*gamma) / ...
         ((1 - alpha)*(1 - gamma)*(1 - tau)*(1 - beta*(1 - delta))));

% Capital del estado estacionario
kss = lss*(alpha*beta*A/(1 - beta*(1 - delta)))^(1/(1 - alpha));

k = linspace(2/3*kss, 1.5*kss, p);
l = linspace(2/3*lss, 1.5*lss, m);       

value = zeros(p*q, p*m, 1);

% Matriz valor: k(t) (capital)
for i = 1:p
    for j = 1:q
        value((i - 1)*q + j, :, 1) = k(i)*ones(1, p*m);
    end
end

% Matriz valor: k(t+1) (capital)
kaux = k(1)*ones(p*q, m);
for i = 2:p
    kaux = [kaux, k(i)*ones(p*q, m)];
end
value(:, :, 2) = kaux;


% Matriz valor: z(t) (shock)
vector = theta;
for i = 1:p - 1
    vector = [vector; theta];
end
value(:, :, 3) = vector*ones(1, p*m);


% Matriz valor: l(t) (trabajo)
laux = l; 
for i = 1:m - 1
    laux = [laux,  l];
end
value(:, :, 4) = ones(p*q, 1)*laux;

% Funci�n de retorno

% La matriz M guarda la funci�n de retorno evaluada en 
% cada posible combinaci�n de (k, z) hoy (variables de estado)
% y (k ma�ana, l hoy) (variables de control).

% Como es una funci�n de utilidad logar�tmica 
% elminamos las celdas inalcanzables con max.

M = (1 - gamma)*...
    log(max( ...
        exp(value(:, :, 3)).*A.*value(:, :, 1).^(alpha).*value(:, :, 4).^(1 - alpha) - ...
        value(:, :, 2) + (1 - delta).*value(:, :, 1), 1e-100)) + ...
    gamma*log(max(1 - value(:, :, 4), 1e-100)); 

I = eye(q);
E = I; 
for i = 1:p - 1
    E = [E; I];
end

%
% Ejecuci�n del algoritmo
%

V0   = zeros(p*q, 1);
fin  = 0;
iter = 1;

while fin == 0 && iter < max_iter
    me = beta*(E*(Pi*reshape(V0, q, p)));
    beta_aux = me(:, 1)*ones(1, m);
    for i = 2:p
        beta_aux = [beta_aux, me(:, i)*ones(1, m)];
    end
	[V1, G1] = max((M + beta_aux)');
	V1 = V1';
	G1 = G1';
    if norm(V0 - V1) < tol
        fin = 1;
    end
    disp(['Iter = ',  num2str(iter), ...
          ' ||V0 - V1|| = ', num2str(norm(V0 - V1))]);
    V0   = V1;
    iter = iter + 1;
end

%
% Extracci�n de resultados
%

G = zeros(p, q);  % Capital
V = zeros(p, q);  % Funci�n valor
H = zeros(m, q);  % Trabajo

%
% Correcciones
%
for i = 1:p
    for j = 1:q
        G(i, j) = floor((G1((i - 1)*q + j))/p);
        V(i, j) = V1((i - 1)*q + j);
    end
end

for i = 1:m
    for j = 1:q
        H(i, j) = mod(G1((i - 1)*q + j), m);
        if H(i, j) == 0
            H(i, j) = m;
        end
    end
end

for j = 1:q
    G(1, j) = 1; 
    V(1, j) = V(2, j);
end

% Consumo para cada estado
c_res = zeros(p, q);
for i = 1:p
    for j = 1:q
        c(i, j) = exp(theta(j))*A*k(i)^alpha*l(H(i, j))^(1 - alpha) - ...
                  k(G(i, j)) + (1 - delta)*k(i);
    end
end 

% Trabajo para cada estado
l_res = zeros(p, q);
for i = 1:p
    for j = 1:q
        l_res(i, j) = l(H(i, j));
    end
end

%
% Gr�ficas
%

figure(1)

subplot(4, 1, 1)
plot(k, k(G(:, 1)), 'b', ...
     k, k(G(:, 2)), 'r', ...
     k, k(G(:, 3)), 'g', ...
     k, k(G(:, 4)), 'c', ...
     k, k(G(:, 5)), 'm')
legend('theta =  - 0.0231', ...
       'theta =  - 0.0115', ...
       'theta = 0', ...
       'theta = 0.0115', ...
       'theta = 0.0231')
title('Capital $\acute{o}$ptimo', ...
      'FontSize', 15, ...
      'interpreter', 'latex')

subplot(4, 1, 2)
plot(k, V(:, 1), 'b', ...
     k, V(:, 2), 'r', ...
     k, V(:, 3), 'g', ...
     k, V(:, 4), 'c', ...
     k, V(:, 5), 'm')
legend('theta =  - 0.0231', ...
       'theta =  - 0.0115', ...
       'theta = 0', ...
       'theta = 0.0115', ...
       'theta = 0.0231')
title('Funci$\acute{o}$n Valor', ...
      'FontSize', 15, ...
      'interpreter', 'latex')

subplot(4, 1, 3)
plot(k, c(:, 1), 'b', ...
     k, c(:, 2), 'r', ...
     k, c(:, 3), 'g', ...
     k, c(:, 4), 'c', ...
     k, c(:, 5), 'm')
legend('theta =  - 0.0231', ...
       'theta =  - 0.0115', ...
       'theta = 0', ...
       'theta = 0.0115', ...
       'theta = 0.0231')
title('Consumo $\acute{O}$ptimo', ...
      'FontSize', 15, ...
      'interpreter', 'latex')

subplot(4, 1, 4)
plot(k, l_res(:, 1), 'b', ...
     k, l_res(:, 2), 'r', ...
     k, l_res(:, 3), 'g', ...
     k, l_res(:, 4), 'c', ...
     k, l_res(:, 5), 'm')
legend('theta =  - 0.0231', ...
       'theta =  - 0.0115', ...
       'theta = 0', ...
       'theta = 0.0115', ...
       'theta = 0.0231')
title('Trabajo $\acute{O}$ptimo', ...
      'FontSize', 15, ...
      'interpreter', 'latex')

%%%%%%%%%%%%%%%    Inciso (b)    %%%%%%%%%%%%%%%

% Distribuci�n invariante
Pi_invariante = Pi^10000;

Ts    = 500;
nsim  = 500;
theta = [1 2 3 4 5];

% Valores obtenidos de Pi_invariante
Pi0   = [0.0362 0.2408 0.4494 0.2379 0.0357];

for m = 1:nsim

    [shocks_stat, states] = markov(theta, Pi, Pi0, Ts);

    kt_stat           = zeros(Ts + 1, 1);
    ind_statk         = zeros(Ts, 1);
    [~, ind_statk(1)] = min(k < kss);
    kt_stat(1)        = k(ind_statk(1));

    lt_stat           = zeros(Ts + 1,1);
    ind_statl         = zeros(Ts,1);
    [~, ind_statl(1)] = min(l < lss);
    lt_stat(1)        = l(ind_statl(1));

    for t = 1:Ts
        ind_statk(t + 1) = G(ind_statk(t), shocks_stat(t));
        kt_stat(t + 1)   = k(ind_statk(t + 1));

        ind_statl(t + 1) = H(ind_statl(t), shocks_stat(t));
        lt_stat(t + 1)   = k(ind_statl(t + 1));
    end

    yt_stat = zeros(Ts, 1);
    ct_stat = zeros(Ts, 1);
    it_stat = zeros(Ts, 1);

    for t = 1:Ts
        yt_stat(t) = exp(theta(shocks_stat(t)))*A*kt_stat(t)^alpha;
        ct_stat(t) = yt_stat(t) - it_stat(t);
        it_stat(t) = kt_stat(t + 1) - (1 - delta)*kt_stat(t);
    end

    yt_std(m) = std(log(yt_stat(100:Ts)));
    ct_std(m) = std(log(ct_stat(100:Ts)));
    it_std(m) = std(log(it_stat(100:Ts)));
    kt_std(m) = std(log(kt_stat(100:Ts)));
    lt_std(m) = std(log(lt_stat(100:Ts)));

    ct_yt_correl(m) = corr(ct_stat(100:Ts), yt_stat(100:Ts));
    it_yt_correl(m) = corr(it_stat(100:Ts), yt_stat(100:Ts));
    kt_yt_correl(m) = corr(kt_stat(100:Ts), yt_stat(100:Ts));
    lt_yt_correl(m) = corr(lt_stat(100:Ts), yt_stat(100:Ts));
end

disp(' ')
disp('Desviacion estandar del logaritmo: ')
disp(' ')

disp(['Producto  = ', num2str(mean(yt_std))])
disp(['Consumo   = ', num2str(mean(ct_std))])
disp(['Inversi�n = ', num2str(mean(it_std))])
disp(['Capital   = ', num2str(mean(kt_std))])
disp(['Trabajo   = ', num2str(mean(lt_std))])

disp(' ')
disp('Correlacion del producto y: ')
disp(' ')
disp(['Consumo   = ', num2str(mean(ct_yt_correl))])
disp(['Inversi�n = ', num2str(mean(it_yt_correl))])
disp(['Capital   = ', num2str(mean(kt_yt_correl))])
disp(['Trabajo   = ', num2str(mean(lt_yt_correl))])
disp(' ')

figure(2)

subplot(4,2,1) 
plot(1:Ts, theta(shocks_stat))
title('Shock Tecnol$\acute{o}$gico', ...
      'interpreter', 'latex')

subplot(4,2,2)
plot(1:Ts, yt_stat)
title('Producto')

subplot(4,2,3)
plot(1:Ts, ct_stat)
title('Consumo')

subplot(4,2,4)
plot(1:Ts, it_stat)
title('Inversi$\acute{o}$n', ...
      'interpreter', 'latex')

subplot(4,2,5)
plot(1:Ts, kt_stat)
title('Capital')

subplot(4,2,6)
plot(1:Ts, lt_stat)
title('Trabajo')

%%%%%%%%%%%%%%%    Inciso (c)    %%%%%%%%%%%%%%%

T = 100;
rho = 0.95;
sigma_eps = 0.0024;

% Realizaci�n de eps
realizacion_eps = normrnd(0, sigma_eps^2);

% Se pide al menos una desviaci�n est�ndar
if abs(realizacion_eps) < sigma_eps
    realizacion_eps = realizacion_eps + sigma_eps;
end

shocks = zeros(T, 1);
shocks(1) = realizacion_eps;

% Construcci�n de los shocks
for i = 2:T
    realizacion_eps = normrnd(0, sigma_eps^2);
    shocks(i) = rho*shocks(i - 1) + realizacion_eps;
end

kt   = zeros(T + 1, 1);
lt   = zeros(T, 1);
indk = zeros(T, 1);
indl = zeros(T, 1);

%% TODO (Omar) Aplicaci�n de los shocks
[~, indk(1)] = min(k < kss);
kt(1) = k(indk(1));

[~, indl(1)] = min(l < lss);
lt(1) = l(indl(1));

for t = 1:T
   indk(t + 1) = G(indk(t), shocks(t));
   kt(t + 1)  = k(indk(t + 1));

   indl(t + 1) = H(indl(t), shocks(t));
   lt(t + 1)  = l(indl(t + 1));
end

% Trabajo para cada estado
l_res = zeros(p, q);
for i = 1:p
    for j = 1:q
        l_res(i, j) = l(H(i, j));
    end
end

%% HASTA AQUI

it = zeros(T, 1);
ct = zeros(T, 1);
yt = zeros(T, 1);

for t = 1:T
    yt(t) = exp(shocks(t))*A*kt(t)^alpha;
    ct(t) = yt(t) - it(t);
    it(t) = kt(t + 1) - (1 - delta)*kt(t);
end

%
% Gr�ficas
%

figure(3)

subplot(3, 2, 1);
plot(1:T, theta(shocks));
title('Shock Tecnol$\acute{o}$gico', ...
      'interpreter', 'latex');

subplot(3, 2, 2);
plot(1:T, yt);
title('Producto');

subplot(3, 2, 3);
plot(1:T, ct);
title('Consumo');

subplot(3, 2, 4)
plot(1:Ts, it)
title('Inversi$\acute{o}$n', ...
      'interpreter', 'latex')

subplot(3, 2, 5);
plot(1:T, kt);
title('Capital');

subplot(3, 2, 6);
plot(1:T, lt);
title('Trabajo');